FROM node:16.15-alpine
WORKDIR /app
COPY package.json ./
# Create app directory
RUN npm install
# Copy app source code
RUN mkdir/root/data

COPY . ./
#Expose port and start application
EXPOSE 3000
CMD [ "npm", "start" ]