import React, { useState } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';

// .carousel-items > img{
//     width: 50% ;
// }


const items = [
  {
    src: '/images/Book1.jpg', width:"2000px" ,height:"2000px",
    altText: 'Book1',
    caption2: 'Project Numer'
  },
  {
    src: '/images/Book2.jpg', width:"200" ,height:"200",
    altText: 'Book2',
    caption: '6104062620098',
    caption2: 'Pitcha Seripanichkarn'
  }
];


const Homepage = (props) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  }

  const slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
      >
        <img src={item.src} alt={item.altText} style={{width: "100%"}} />
        <CarouselCaption captionText={item.caption} captionHeader={item.caption2} />
      </CarouselItem>
    );
  });

  return (
    <div className="container">
    <Carousel
      activeIndex={activeIndex}
      next={next}
      previous={previous}
    >
      <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
      {slides}
      <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
      <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
    </Carousel>
    </div>
  );
 }

export default Homepage;
