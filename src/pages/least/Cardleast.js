import React from 'react';
import {
  Card, Button, CardImg, CardTitle, CardText, CardGroup, CardSubtitle, CardBody, CardDeck
} from 'reactstrap';
import { Link } from 'react-router-dom'

const Cardleast = (props) => {
  return (
    <div className="container">
    <CardDeck>
      <Card>
        <CardImg top width="100%" src="https://s.isanook.com/he/0/ud/4/22653/cat-mask.jpg"/>
        <CardBody>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Linear Regression</CardSubtitle>
          <CardText></CardText>
          <Link to="/LinearRe" activeClassName="is-active">
          <Button renderAs="button"><span>Submit</span></Button>
          </Link>
        </CardBody>
      </Card>
      <div>
        <h1>&nbsp;</h1>
    </div>
      <Card>
        <CardImg top width="100%" src="https://s.isanook.com/he/0/ud/4/22653/cat-mask.jpg"  />
        <CardBody>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Polynomials Regression</CardSubtitle>
          <CardText></CardText>
          <Link to="/polynomials" activeClassName="is-active">
          <Button renderAs="button"><span>Submit</span></Button>
          </Link>
        </CardBody>
      </Card>
      <div>
        <h1>&nbsp;</h1>
    </div>
      <Card>
        <CardImg top width="100%" src="https://s.isanook.com/he/0/ud/4/22653/cat-mask.jpg" />
        <CardBody>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Multiple Linear Regression</CardSubtitle>
          <CardText></CardText>
          <Link to="/Multiplelinear" activeClassName="is-active">
          <Button renderAs="button"><span>Submit</span></Button>
          </Link>
        </CardBody>
      </Card>
    </CardDeck>
    </div>
  );
};

export default Cardleast;